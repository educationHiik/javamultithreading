
package ru.hiik;

/**
 *
 * @author developer
 */
public class JavaThread {

    
    public static void main(String[] args) throws InterruptedException {
      
        PrintThread t1 = new PrintThread();
        PrintThread t2 = new PrintThread();
        
        t1.getRunning().set(true);
        
        t1.start();
        t2.start();
        
        Thread.sleep(10000);
        t1.getRunning().set(false);
        
        
    }
    
}
