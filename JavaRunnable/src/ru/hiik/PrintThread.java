/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.hiik;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author developer
 */
public class PrintThread extends Thread{

   
    //
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-yyyy-MM HH:mm:ss");
    
    
    private AtomicBoolean running = new AtomicBoolean(false);
    
    @Override
    public void run() {
        
        while (running.get())
        {
            LocalDateTime ldt = LocalDateTime.now();
            System.out.println("["+ldt.format(formatter)+"] Выполнение нити: "+this.getName());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException ex) {
                Logger.getLogger(PrintThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }    
        
    }
    
   
    public AtomicBoolean getRunning() {
        return running;
    }

   
    public void setRunning(AtomicBoolean running) {
        this.running = running;
    }

    
}
